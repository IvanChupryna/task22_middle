function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

function createTable() {
    var existingTables = document.getElementsByTagName('table');
    for(table of existingTables){
        table.remove();
    }

    var table = document.createElement('table');
    for(var i = 0; i < document.getElementById('rows').value; i++){
        var newRow = document.createElement('tr');
        newRow.className = "row";
        for(var j = 0; j < document.getElementById('cols').value; j++){
            const newCol = document.createElement('td');
            const defaultColor  = newCol.style.backgroundColor;

            newCol.className = "cell";
            newCol.innerHTML = (i + 1).toString().concat((j + 1).toString());
            newCol.addEventListener('click', function(){
                changeColor(newCol, defaultColor);
            });

            newRow.appendChild(newCol);
        }
        table.className = "current_table";
        table.appendChild(newRow);
    }
    document.body.appendChild(table);
}

function changeColor(element, defaultColor){
    if(element.style.backgroundColor == defaultColor)
    {
        element.style.backgroundColor = getRandomColor();
    }
    else
    {
        element.style.backgroundColor = defaultColor;
    }
}

var element = document.getElementById('create_table');
element.addEventListener('click', createTable);